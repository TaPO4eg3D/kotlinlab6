package com.denis.fomin.criminalintent.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.LocalDate

@Entity
data class CriminalIntent(
    @PrimaryKey(
        autoGenerate = true,
    ) val id: Int = 0,
    @ColumnInfo() val title: String,
    @ColumnInfo() val created: LocalDate,
    @ColumnInfo() val isSolved: Boolean = false,
)