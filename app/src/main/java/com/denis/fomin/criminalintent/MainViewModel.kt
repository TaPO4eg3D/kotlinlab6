package com.denis.fomin.criminalintent

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.denis.fomin.criminalintent.daos.CriminalIntentDAO
import com.denis.fomin.criminalintent.models.CriminalIntent
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

data class AppState(
    val selectedItem: CriminalIntent? = null,
)

class MainViewModel(app: Application): AndroidViewModel(app) {
    private val dao: CriminalIntentDAO
    private val _appState = MutableStateFlow(AppState())

    val allItems: LiveData<List<CriminalIntent>>
    val appState: StateFlow<AppState> = _appState.asStateFlow()

    init {
        dao = ApplicationDatabase
            .getDatabase(app)
            .criminalIntentDao()

        allItems = dao.getAll()
    }

    fun setSelectedItem(item: CriminalIntent?) {
        _appState.update { currentState ->
            currentState.copy(
                selectedItem = item,
            )
        }
    }

    fun createNewCriminalIntent(item: CriminalIntent) {
        dao.createItem(item)
    }

    fun updateCriminalIntent(item: CriminalIntent) {
        dao.updateItem(item)
    }

    fun deleteCriminalIntent(item: CriminalIntent) {
        dao.delete(item)

        _appState.update {currentState ->
            currentState.copy(
                selectedItem = null
            )
        }
    }
}