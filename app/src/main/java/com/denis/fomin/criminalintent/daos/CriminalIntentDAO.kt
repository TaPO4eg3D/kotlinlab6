package com.denis.fomin.criminalintent.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.denis.fomin.criminalintent.models.CriminalIntent

@Dao
interface CriminalIntentDAO {
    @Query(
        "SELECT * FROM CriminalIntent"
    )
    fun getAll(): LiveData<List<CriminalIntent>>

    @Query(
        "SELECT * FROM CriminalIntent " +
        "WHERE title LIKE :title LIMIT :limit"
    )
    fun findByTitle(title: String, limit: Int = 5): CriminalIntent?

    @Insert
    fun createItem(item: CriminalIntent): Long

    @Update
    fun updateItem(item: CriminalIntent)

    @Delete
    fun delete(item: CriminalIntent)
}