package com.denis.fomin.criminalintent

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.windowsizeclass.ExperimentalMaterial3WindowSizeClassApi
import androidx.compose.material3.windowsizeclass.WindowSizeClass
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.material3.windowsizeclass.calculateWindowSizeClass
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.denis.fomin.criminalintent.models.CriminalIntent
import com.denis.fomin.criminalintent.ui.theme.CriminalIntentTheme
import com.vanpra.composematerialdialogs.MaterialDialog
import com.vanpra.composematerialdialogs.datetime.date.datepicker
import com.vanpra.composematerialdialogs.rememberMaterialDialogState
import java.time.LocalDate

class MainActivity : ComponentActivity() {
    @OptIn(ExperimentalMaterial3WindowSizeClassApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CriminalIntentTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val windowSizeClass = calculateWindowSizeClass(this)
                    CriminalIntentApplication(
                        windowSizeClass = windowSizeClass,
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DetailedCriminalIntent(
    title: String,
    isSolved: Boolean,
    created: LocalDate,
    onTitleChange: (value: String) -> Unit,
    onIsSolvedChange: (value: Boolean) -> Unit,
    onCreatedChange: (value: LocalDate) -> Unit,
) {
    val dialogState = rememberMaterialDialogState()
    MaterialDialog(
        dialogState = dialogState,
        buttons = {
            positiveButton("Ok")
            negativeButton("Cancel")
        }
    ) {
        datepicker(
            initialDate = created,
        ) { date ->
            onCreatedChange(date)
        }
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(20.dp)
    ) {
        Text(
            text = "TITLE",
            fontWeight = FontWeight.Bold,
            fontSize = 18.sp,
            modifier = Modifier.padding(bottom = 5.dp)
        )
        Divider(thickness = 1.dp, color = Color.Black)
        TextField(
            value = title,
            placeholder = {
                Text(
                    text = "Enter a title for the crime"
                )
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 10.dp),
            onValueChange = onTitleChange,
        )
        Text(
            text = "DETAILS",
            fontWeight = FontWeight.Bold,
            fontSize = 18.sp,
            modifier = Modifier.padding(bottom = 5.dp, top = 20.dp)
        )
        Divider(thickness = 1.dp, color = Color.Black)
        Button(
            onClick = {
                dialogState.show()
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    vertical = 10.dp
                ),
            shape = RectangleShape,
        ) {
            Text(text = created.toString())
        }
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Checkbox(
                checked = isSolved,
                onCheckedChange = onIsSolvedChange,
            )
            Text(
                text = "Solved",
                fontSize = 18.sp,
            )
        }
    }
}

@Composable
fun CriminalIntentListItem(
    item: CriminalIntent,
    modifier: Modifier = Modifier,
    onClick: (item: CriminalIntent) -> Unit,
) {
    val fullWidth = modifier.fillMaxWidth()

    TextButton(
        onClick = {
            onClick(item)
        },
        modifier = fullWidth,
    ) {
        Column(
            modifier = fullWidth,
        ) {
            Text(
                text = item.title,
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold,
            )
            Text(
                text = item.created.toString(),
                fontSize = 15.sp,
            )
        }
    }
}

@Composable
fun CriminalIntentList(
    items: List<CriminalIntent>,
    modifier: Modifier = Modifier,
    onSelect: (item: CriminalIntent) -> Unit,
) {
    Column(
        modifier = modifier.padding(20.dp),
    ) {
        for (item in items) {
            CriminalIntentListItem(
                item = item,
                onClick = onSelect,
            )
        }
    }
}

@Composable
fun CriminalIntentListScreen(
    modifier: Modifier = Modifier,
    viewModel: MainViewModel = viewModel(),
    onSelect: (item: CriminalIntent) -> Unit,
    onCreate: () -> Unit,
) {
    val items by viewModel.allItems.observeAsState()

    Column(
        modifier = modifier,
    ) {
        ApplicationTopBar("All Cases") {
            IconButton(onClick = onCreate) {
                Icon(Icons.Filled.Add, null)
            }
        }
        if (items != null) {
            CriminalIntentList(
                items = items!!,
                onSelect = onSelect,
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ApplicationTopBar(
    title: String = "Criminal Intent",
    actions: @Composable RowScope.() -> Unit = {},
) {
    TopAppBar(
        title = {
            Text(title)
        },
        actions = actions,
    )
}

@Composable
fun CriminalIntentHomeCompactScreen(
    viewModel: MainViewModel,
    navController: NavController,
) {
    CriminalIntentListScreen(
        viewModel = viewModel,
        onSelect = { item ->
            viewModel.setSelectedItem(item)
            navController.navigate("details")
        },
        onCreate = {
            navController.navigate("createNewItem")
        }
    )
}

@Composable
fun CriminalIntentHomeExtendedScreen(
    showCreate: Boolean,
    showDetails: Boolean,
    viewModel: MainViewModel,
    navController: NavController,
) {
    Row() {
        CriminalIntentListScreen(
            viewModel = viewModel,
            onSelect = { item ->
                viewModel.setSelectedItem(item)
                navController.navigate("details")
            },
            onCreate = {
                navController.navigate("createNewItem")
            },
            modifier = Modifier.widthIn(max = 300.dp)
        )

        if (showCreate) {
            CriminalIntentNewItemScreen(
                viewModel = viewModel,
                navController = navController,
            )
        } else if (showDetails) {
            CriminalIntentDetailsScreen(
                viewModel = viewModel,
                navController = navController,
            )
        } else {
            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Text(
                    text = "No item selected",
                    fontWeight = FontWeight.Bold,
                    fontSize = 20.sp,
                )
            }
        }
    }
}

@Composable
fun CriminalIntentDetailsScreen(
    viewModel: MainViewModel,
    navController: NavController,
) {
    val appState by viewModel.appState.collectAsStateWithLifecycle()
    val selectedItem = appState.selectedItem!!

    var title by rememberSaveable { mutableStateOf(selectedItem.title) }
    var isSolved by rememberSaveable { mutableStateOf(selectedItem.isSolved) }
    var created by rememberSaveable { mutableStateOf(selectedItem.created) }

    var showConfirmation by rememberSaveable { mutableStateOf(false) }

    if (showConfirmation) {
        AlertDialog(
            onDismissRequest = {
                showConfirmation = false
            },
            confirmButton = {
                Button(onClick = {
                    viewModel.deleteCriminalIntent(
                        CriminalIntent(
                            id = selectedItem.id,
                            title = title,
                            isSolved = isSolved,
                            created = created,
                        ))

                    navController.navigate("home")
                    showConfirmation = false
                }) {
                    Text("YES")
                }
            },
            dismissButton = {
                Button(onClick = {
                    showConfirmation = false
                }) {
                    Text("NO")
                }
            },
            title = {
                Text("Are you sure?")
            },
            text = {
                Text(
                    "This action will delete your record without possibility " +
                            "to recover it"
                )
            }
        )
    }

    Column {
        ApplicationTopBar("Details | Update") {
            IconButton(
                onClick = {
                    showConfirmation = true
                }
            ) {
                Icon(Icons.Filled.Delete, null)
            }
            IconButton(
                enabled = title.isNotEmpty(),
                onClick = {
                    viewModel.updateCriminalIntent(
                        CriminalIntent(
                            id = appState.selectedItem!!.id,
                            title = title,
                            isSolved = isSolved,
                            created = created,
                        ))

                    navController.navigate("home")
                }
            ) {
                Icon(Icons.Filled.Check, null)
            }
        }
        DetailedCriminalIntent(
            title = title,
            isSolved = isSolved,
            created = created,
            onTitleChange = { title = it },
            onIsSolvedChange = { isSolved = it },
            onCreatedChange = { created = it }
        )
    }
}

@Composable
fun CriminalIntentNewItemScreen(
    viewModel: MainViewModel,
    navController: NavController,
    modifier: Modifier = Modifier
) {
    var title by rememberSaveable { mutableStateOf("") }
    var isSolved by rememberSaveable { mutableStateOf(false) }
    var created by rememberSaveable { mutableStateOf(LocalDate.now()) }

    Column(
        modifier = modifier,
    ) {
        ApplicationTopBar(
            title = "Create New Item",
        ) {
            IconButton(
                enabled = title.isNotEmpty(),
                onClick = {
                    viewModel.createNewCriminalIntent(
                        CriminalIntent(
                        title = title,
                        isSolved = isSolved,
                        created = created,
                    ))

                    navController.navigate("home")
                }
            ) {
                Icon(Icons.Filled.Check, null)
            }
        }
        DetailedCriminalIntent(
            title = title,
            isSolved = isSolved,
            created = created,
            onTitleChange = { title = it },
            onIsSolvedChange = { isSolved = it },
            onCreatedChange = { created = it }
        )
    }
}

@Composable
fun CriminalIntentApplication(
    navController: NavHostController = rememberNavController(),
    startDestination: String = "home",
    viewModel: MainViewModel = viewModel(),
    windowSizeClass: WindowSizeClass,
) {
    val showExpanded = windowSizeClass.widthSizeClass == WindowWidthSizeClass.Expanded

    NavHost(
        navController = navController,
        startDestination = startDestination,
    ) {
        if (showExpanded) {
            composable("home") {
                CriminalIntentHomeExtendedScreen(
                    viewModel = viewModel,
                    navController = navController,
                    showCreate = false,
                    showDetails = false,
                )
            }
            composable("details") {
                CriminalIntentHomeExtendedScreen(
                    viewModel = viewModel,
                    navController = navController,
                    showCreate = false,
                    showDetails = true,
                )
            }
            composable("createNewItem") {
                CriminalIntentHomeExtendedScreen(
                    viewModel = viewModel,
                    navController = navController,
                    showCreate = true,
                    showDetails = false,
                )
            }
        } else {
            composable("home") {
                CriminalIntentHomeCompactScreen(
                    viewModel = viewModel,
                    navController = navController,
                )
            }
            composable("details") {
                CriminalIntentDetailsScreen(
                    viewModel = viewModel,
                    navController = navController,
                )
            }
            composable("createNewItem") {
                CriminalIntentNewItemScreen(
                    viewModel = viewModel,
                    navController = navController,
                )
            }
        }
    }
}